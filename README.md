# docker-remco

Basic image with remco installed

## Usage

```dockerfile
FROM registry.gitlab.com/andrewheberle/docker-remco:debian10

COPY your/remco/config /etc/remco/

# the rest of your image
```

## Configuration

The image includes a very basic config for remco however you will need to add your own
to do anything useful.