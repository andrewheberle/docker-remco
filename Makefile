REMCO_VERSION = 0.12.4
BASE_TAG ?= latest
TAG ?= latest

ifndef IMAGE
$(error IMAGE must be provided)
endif

build-%: Dockerfile-%
	docker build -t "$(IMAGE):$*" --build-arg REMCO_VERSION=$(REMCO_VERSION) --build-arg BASE_TAG=$(BASE_TAG) -f Dockerfile-$* .

release-%: Dockerfile-% build-%
	docker tag "$(IMAGE):$*" "$(IMAGE):$(TAG)-$*"
	docker push "$(IMAGE):$*"
	docker push "$(IMAGE):$(TAG)-$*"

clean:
	$(RM) Dockerfile-alpine
	$(RM) Dockerfile-debian
	$(RM) Dockerfile-ubuntu
